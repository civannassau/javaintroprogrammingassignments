/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section4_oop.part1_inheritance;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */

public class Animal {
    private final String name;
   // private int age;
    private String movementtype;
    private double speed;

    public Animal(String name) {
                this.name = name;
              //  this.age = age;
    }

    
    /**
     * returns the name of the animal
     * @return name the species name
     */
    public String getName() {
        //YOUR CODE HERE (and remove the throw statement)
       // throw new UnsupportedOperationException("Not implemented yet");
        return name;
    }
    
    /**
     * returns the movement type
     * @return movementType the way the animal moves
     */
    public String getMovementType() {
        //YOUR CODE HERE (and remove the throw statement)
       // throw new UnsupportedOperationException("Not implemented yet");
        return movementtype;
    }
    
    /**
     * returns the speed of this animal
     * @return speed the speed of this animal
     */
    public double getSpeed() {
        //YOUR CODE HERE (and remove the throw statement)
        //throw new UnsupportedOperationException("Not implemented yet");
        return speed;
    }
    
}
