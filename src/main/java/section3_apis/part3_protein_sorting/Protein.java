/*
 * Copyright (c) 2015 Michiel Noback [michiel.noback@gmail.com].
 * All rights reserved.
 */

package section3_apis.part3_protein_sorting;

import java.util.Comparator;

/**
 *
 * @author Michiel Noback [michiel.noback@gmail.com]
 * @version 0.0.1
 */
public class Protein implements Comparable<Protein> {
    private final String name;
    private final String accession;
    private final String aminoAcidSequence;
    private GOannotation goAnnotation;

    /**
     * constructs without GO annotation;
     * @param name the protein name
     * @param accession the accession number
     * @param aminoAcidSequence the proteins amino acid sequence
     */
    public Protein(String name, String accession, String aminoAcidSequence) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
    }

    /**
     * constructs with main properties.
     * @param name the protein name
     * @param accession the accession number
     * @param aminoAcidSequence the proteins amino acid sequence
     * @param goAnnotation the GO annotation
     */
    public Protein(String name, String accession, String aminoAcidSequence, GOannotation goAnnotation) {
        this.name = name;
        this.accession = accession;
        this.aminoAcidSequence = aminoAcidSequence;
        this.goAnnotation = goAnnotation;
    }

    @Override
    public int compareTo(Protein other) {
        //YOUR CODE HERE (and remove the throw statement)
        return this.name.compareTo(other.name);
    }

    /**
     * provides a range of possible sorters, based on the type that is requested.
     * @param type the sorting type that is required
     * @return proteinSorter
     */
    public static Comparator<Protein> getSorter(SortingType type) {
        //YOUR CODE HERE (and remove the throw statement)

        if (SortingType.PROTEIN_NAME.equals(type)) {
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein t1) {
                    return protein.name.compareTo(t1.name);
                }
            };

        } else if(SortingType.ACCESSION_NUMBER .equals(type)) {
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein t1) {
                    return protein.accession.toLowerCase().compareTo(t1.accession.toLowerCase());
                }
            };

        } else if(SortingType.GO_ANNOTATION.equals(type)){
            return new Comparator<Protein>() {
                @Override
                public int compare(Protein protein, Protein t1) {
                    int c;
                    c = protein.goAnnotation.getBiologicalProcess().compareTo(t1.goAnnotation.getBiologicalProcess());
                    if (c==0) {
                        c = protein.goAnnotation.getCellularComponent().compareTo(t1.goAnnotation.getCellularComponent());
                    }
                    if (c==0) {
                        c = protein.goAnnotation.getMolecularFunction().compareTo(t1.goAnnotation.getMolecularFunction());
                    }
                    return c;
                }

            };
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     *
     * @return name the protein name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return accession the accession number
     */
    public String getAccession() {
        return accession;
    }

    /**
     *
     * @return aminoAcidSequence the amino acid sequence
     */
    public String getAminoAcidSequence() {
        return aminoAcidSequence;
    }

    /**
     *
     * @return GO annotation
     */
    public GOannotation getGoAnnotation() {
        return goAnnotation;
    }

    @Override
    public String toString() {
        return "Protein{" + "name=" + name + ", accession=" + accession + ", aminoAcidSequence=" + aminoAcidSequence + '}';
    }

}