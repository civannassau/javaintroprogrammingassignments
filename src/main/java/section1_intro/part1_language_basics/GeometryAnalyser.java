package section1_intro.part1_language_basics;

import java.text.NumberFormat;

public class GeometryAnalyser {
    public static void main(String[] args) {
        //YOUR CODE HERE
        Point point1 = new Point();
        point1.x = Integer.parseInt(args[0]);
        point1.y = Integer.parseInt(args[1]);

        Point point2 = new Point();
        point2.x = Integer.parseInt(args[2]);
        point2.y = Integer.parseInt(args[3]);

        Rectangle square = new Rectangle();
        square.upperLeft = point1;
        square.lowerRight = point2;


        String method = args[4];
        switch (method) {
            case "surf":
                System.out.println(square.getSurface());
                break;
            case "dist":
                NumberFormat numberFormat = NumberFormat.getNumberInstance();
                numberFormat.setMaximumFractionDigits(1);
                System.out.println(numberFormat.format(point1.euclideanDistanceTo(point2)));
                break;
        }
    }
}

