package section1_intro.part1_language_basics;


public class Point {
    int x;
    int y;

    /**
     * Create an instance of class point that is located at the same coordinates as the current object, but in the
     * diagonally opposing quadrant of coordinate space.
     * So, when the current point is at (4, 4), this method will return Point(-4, -4)
     * and when the current point is at (2, -5) it will return Point(-2, 5).
     * @return inverse Point
     */
    Point createInversePoint() {
        //maak new point
        Point revpoint = new Point();
         revpoint.x= x*-1;
         revpoint.y = y*-1;
        return revpoint;
    }

    /**
     * This method returns the Euclidean distance of the current point (this) to the given point (otherPoint).
     * GIYF if you forgot what Euclidean distance is and how it is calculated.
     * @param otherPoint
     * @return euclidean distance
     */
    double euclideanDistanceTo(Point otherPoint) {
        //YOUR CODE HERE
        int x2 = Math.abs(x - otherPoint.x);
        int y2 = Math.abs(y - otherPoint.y);
        double euclideanDistanceTo = Math.sqrt(Math.pow(y2, 2.0) + Math.pow(x2, 2.0));

        return euclideanDistanceTo;
//        return 0;
    }
}
